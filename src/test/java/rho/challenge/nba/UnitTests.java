package rho.challenge.nba;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import rho.challenge.nba.config.ApplicationConfig;
import rho.challenge.nba.config.CacheConfig;
import rho.challenge.nba.constants.Constants;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.repository.NBAMatchCommentRepository;
import rho.challenge.nba.repository.NBAMatchRepository;
import rho.challenge.nba.service.NBAMatchesService;
import rho.challenge.nba.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = {NBAMatchesService.class, ApplicationConfig.class, CacheConfig.class}
)
@TestPropertySource(
		  locations = "classpath:application-integrationtest.properties")
public class UnitTests {

	@Autowired
	private NBAMatchesService service;
	
	@MockBean
	private NBAMatchCommentRepository matchCommentRep;
	
	@MockBean
	private NBAMatchRepository matchRep;
	
	@Test
	public void getAllNBAMatches() throws Exception {
		final Pageable pageable = TestUtils.generatePageable();
		final NBAMatch match = Constants.DEFAULT_MATCH;
		
		Mockito.when(matchRep.findByGameDate(Constants.DEFAULT_QUERY_DATE, pageable)).thenReturn(new PageImpl<>(List.of(match)));
		
		List<NBAMatch> result = service.getAllNBAMatches(Constants.DEFAULT_CTX, Constants.DEFAULT_QUERY_DATE_VAL, 
				Constants.DEFAULT_QUERY_PAGE, Constants.DEFAULT_QUERY_PAGE_SIZE);
		Assert.assertFalse("No NBA Matches were returned.", result.isEmpty());
		NBAMatch resultEntry = result.get(0);
		
		Assert.assertEquals("The NBAMatch returned was not of the same date that was requested.", resultEntry.getGameDate(), match.getGameDate());
		
		// check that player with score 0 is removed:
		Assert.assertNull("Player with score 0 was included in the list.", TestUtils.findPlayerScoreWithNoScore(resultEntry));
		Assert.assertEquals("Total scores for home team was not the expected!", 
				match.getHomeTeam().getTotalScore(), resultEntry.getHomeTeam().getTotalScore());
		Assert.assertEquals("Total scores for visitor team was not the expected!", 
				match.getVisitorTeam().getTotalScore(), resultEntry.getVisitorTeam().getTotalScore());
	}
	
	@Test
	public void getAllNBAMatchesNoPagination() throws Exception {
		final NBAMatch match = Constants.DEFAULT_MATCH;
		
		Mockito.when(matchRep.findByGameDate(Constants.DEFAULT_QUERY_DATE)).thenReturn(List.of(match));
		
		List<NBAMatch> result = service.getAllNBAMatches(Constants.DEFAULT_CTX, Constants.DEFAULT_QUERY_DATE_VAL, -1, -1);
		
		Assert.assertFalse("No NBA Matches were returned.", result.isEmpty());
		NBAMatch resultEntry = result.get(0);
		
		Assert.assertEquals("The NBAMatch returned was not of the same date that was requested.", resultEntry.getGameDate(), match.getGameDate());
		
		// check that player with score 0 is removed:
		Assert.assertNull("Player with score 0 was included in the list.", TestUtils.findPlayerScoreWithNoScore(resultEntry));
		Assert.assertEquals("Total scores for home team was not the expected!", 
				match.getHomeTeam().getTotalScore(), resultEntry.getHomeTeam().getTotalScore());
		Assert.assertEquals("Total scores for visitor team was not the expected!", 
				match.getVisitorTeam().getTotalScore(), resultEntry.getVisitorTeam().getTotalScore());
	}
	
	@Test
	public void getAllNBAMatchesInvalidInput() throws Exception {
		Assert.assertThrows("Invalid date did not raise exception.", NBAServiceException.class, () -> {
			service.getAllNBAMatches(Constants.DEFAULT_CTX, null, -1, -1);
		});
	}
	
	@Test
	public void getSingleNBAMatch() throws Exception {
		final NBAMatch match = Constants.DEFAULT_MATCH;
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		
		NBAMatch result = service.getSingleNBAMatch(Constants.DEFAULT_CTX, match.getGameId());
		Assert.assertNotNull("No NBA Match returned", result);
		
		Assert.assertEquals("The NBAMatch returned was not of the same id that was requested.", result.getGameId(), match.getGameId());
		
		// check that player with score 0 is removed:
		Assert.assertNull("Player with score 0 was included in the list.", TestUtils.findPlayerScoreWithNoScore(result));
		Assert.assertEquals("Total scores for home team was not the expected!", 
				match.getHomeTeam().getTotalScore(), result.getHomeTeam().getTotalScore());
		Assert.assertEquals("Total scores for visitor team was not the expected!", 
				match.getVisitorTeam().getTotalScore(), result.getVisitorTeam().getTotalScore());
	}
	
	@Test
	public void getSingleNBAMatchInvalidInput() throws Exception {
		Assert.assertThrows("Invalid game id did not raise exception.", NBAServiceException.class, () -> {
			service.getSingleNBAMatch(Constants.DEFAULT_CTX, null);
		});
	}
	
	@Test
	public void addNBAMatchComment() throws Exception {
		final String commentToAdd = Constants.DEFAULT_ADD_COMMENT;
		
		final NBAMatch match = new NBAMatch();
		match.setGameId(UUID.randomUUID().toString());
		match.setGameDate(Constants.DEFAULT_QUERY_DATE);
		
		final NBAMatchComment matchComment = new NBAMatchComment();
		matchComment.setGameId(match.getGameId());
		matchComment.setDate(new Date());
		matchComment.setComment(commentToAdd);
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		Mockito.when(matchCommentRep.insert(matchComment)).thenReturn(matchComment);
		
		final NBAMatchComment newComment = service.addNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), commentToAdd);
		Assert.assertEquals("The returned NBAComment's value was not the requested.", commentToAdd, newComment.getComment());
		Assert.assertEquals("The deleted NBAComment's game id was not the same.", matchComment.getGameId(), newComment.getGameId());
	}
	
	@Test
	public void addNBAMatchCommentInvalidInput() throws Exception {
		Assert.assertThrows("Invalid game id did not raise exception.", NBAServiceException.class, () -> {
			service.addNBAMatchComment(Constants.DEFAULT_CTX, null, null);
		});
	}
	
	@Test
	public void updateNBAMatchComment() throws Exception {
		final String commentToUpdate = Constants.DEFAULT_ADD_COMMENT;
		final String newComment = Constants.DEFAULT_UPDATE_COMMENT;
		
		final NBAMatch match = new NBAMatch();
		match.setGameId(UUID.randomUUID().toString());
		match.setGameDate(Constants.DEFAULT_QUERY_DATE);
		
		final NBAMatchComment matchComment = new NBAMatchComment();
		matchComment.setId(UUID.randomUUID().toString());
		matchComment.setGameId(match.getGameId());
		matchComment.setDate(new Date());
		matchComment.setComment(commentToUpdate);
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		Mockito.when(matchCommentRep.findById(matchComment.getId())).thenReturn(Optional.of(matchComment));
		
		final NBAMatchComment updatedComment = service.updateNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), matchComment.getId(), newComment);
		Assert.assertEquals("The deleted NBAComment's value was not the updated.", newComment, updatedComment.getComment());
		Assert.assertEquals("The deleted NBAComment's game id was not the same.", matchComment.getGameId(), updatedComment.getGameId());
		Assert.assertEquals("The deleted NBAComment's id was not the same.", matchComment.getId(), updatedComment.getId());
	}
	
	@Test
	public void updateNBAMatchCommentInvalidInput() throws Exception {
		Assert.assertThrows("Invalid game id did not raise exception.", NBAServiceException.class, () -> {
			service.updateNBAMatchComment(Constants.DEFAULT_CTX, null, null, null);
		});
		
		Assert.assertThrows("Invalid comment id did not raise exception.", NBAServiceException.class, () -> {
			service.updateNBAMatchComment(Constants.DEFAULT_CTX, "", null, null);
		});
		
		final NBAMatch match = Constants.DEFAULT_MATCH;
		final String commentId = UUID.randomUUID().toString();
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.empty());
		Assert.assertThrows("Unexistent match did not raise exception.", NBAServiceException.class, () -> {
			service.updateNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), commentId, null);
		});
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		Mockito.when(matchCommentRep.findById(commentId)).thenReturn(Optional.empty());
		
		Assert.assertThrows("Unexistent comment did not raise exception.", NBAServiceException.class, () -> {
			service.updateNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), commentId, null);
		});
	}
	
	@Test
	public void deleteNBAMatchComment() throws Exception {
		final String commentToAdd = Constants.DEFAULT_ADD_COMMENT;
		
		final NBAMatch match = new NBAMatch();
		match.setGameId(UUID.randomUUID().toString());
		match.setGameDate(Constants.DEFAULT_QUERY_DATE);
		
		final NBAMatchComment matchComment = new NBAMatchComment();
		matchComment.setId(UUID.randomUUID().toString());
		matchComment.setGameId(match.getGameId());
		matchComment.setDate(new Date());
		matchComment.setComment(commentToAdd);
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		Mockito.when(matchCommentRep.findById(matchComment.getId())).thenReturn(Optional.of(matchComment));
		
		final NBAMatchComment deletedComment = service.deleteNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), matchComment.getId());
		Assert.assertEquals("The deleted NBAComment's value was not the same.", commentToAdd, deletedComment.getComment());
		Assert.assertEquals("The deleted NBAComment's game id was not the same.", matchComment.getGameId(), deletedComment.getGameId());
		Assert.assertEquals("The deleted NBAComment's id was not the same.", matchComment.getId(), deletedComment.getId());
	}
	
	@Test
	public void deleteNBAMatchCommentInvalidInput() throws Exception {
		Assert.assertThrows("Invalid game id did not raise exception.", NBAServiceException.class, () -> {
			service.deleteNBAMatchComment(Constants.DEFAULT_CTX, null, null);
		});
		
		Assert.assertThrows("Invalid comment id did not raise exception.", NBAServiceException.class, () -> {
			service.deleteNBAMatchComment(Constants.DEFAULT_CTX, "", null);
		});
		
		final NBAMatch match = Constants.DEFAULT_MATCH;
		final String commentId = UUID.randomUUID().toString();
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.empty());
		Assert.assertThrows("Unexistent match did not raise exception.", NBAServiceException.class, () -> {
			service.deleteNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), commentId);
		});
		
		Mockito.when(matchRep.findById(match.getGameId())).thenReturn(Optional.of(match));
		Mockito.when(matchCommentRep.findById(commentId)).thenReturn(Optional.empty());
		
		Assert.assertThrows("Unexistent comment did not raise exception.", NBAServiceException.class, () -> {
			service.deleteNBAMatchComment(Constants.DEFAULT_CTX, match.getGameId(), commentId);
		});
	}
}
