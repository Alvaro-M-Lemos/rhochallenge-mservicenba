package rho.challenge.nba.utils;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import rho.challenge.nba.constants.Constants;
import rho.challenge.nba.controller.response.Nbaresponse;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.model.NBAMatchTeam;
import rho.challenge.nba.model.NBAPlayerScore;

public class TestUtils {
	public static NBAMatchComment findMatchComment(List<NBAMatchComment> comments, String id, String gameId) {
		for (NBAMatchComment comment : comments) {
			final boolean idMatches = id == null || comment.getId().equals(id);
			final boolean gameIdMatches = gameId == null || comment.getGameId().equals(gameId);
			
			if (idMatches && gameIdMatches) {
				return comment;
			}
		}
		
		return null;
	}

	public static Nbaresponse findNBAMatchWithId(String gameId, List<Nbaresponse> responseList) {
		for (Nbaresponse resp : responseList) {
			if (resp.getGameId().equals(gameId)) {
				return resp;
			}
		}
		return null;
	}

	public static PageRequest generatePageable() {
		return PageRequest.of(Constants.DEFAULT_QUERY_PAGE, Constants.DEFAULT_QUERY_PAGE_SIZE);
	}

	public static NBAMatch generateSampleMatch(String homeTeamName, int homeTotalScore, 
			String awayTeamName, int awayTotalScore, 
			NBAPlayerScore[] homeScores, NBAPlayerScore[] awayScores) {
		final NBAMatch match = new NBAMatch();
		match.setGameId(UUID.randomUUID().toString());
		match.setGameDate(Constants.DEFAULT_QUERY_DATE);
		
		match.setHomeTeam(new NBAMatchTeam());
		match.setVisitorTeam(new NBAMatchTeam());
		
		match.getHomeTeam().setName(homeTeamName);
		match.getHomeTeam().setPlayerScores(new LinkedList<NBAPlayerScore>());
		match.getVisitorTeam().setName(awayTeamName);
		match.getVisitorTeam().setPlayerScores(new LinkedList<NBAPlayerScore>());
		
		addScoresForTeam(match.getHomeTeam(), homeScores);
		addScoresForTeam(match.getVisitorTeam(), awayScores);
		
		return match;
	}

	private static void addScoresForTeam(NBAMatchTeam homeTeam, NBAPlayerScore[] scores) {
		for (NBAPlayerScore score : scores) {
			homeTeam.getPlayerScores().add(score);
		}
	}
	
	public static NBAPlayerScore[] generateScores(List<Map.Entry<String, Integer>> scores) {
		final NBAPlayerScore[] result = new NBAPlayerScore[scores.size()];
		
		final Iterator<Map.Entry<String, Integer>> scoresI = scores.iterator();
		for (int k = 0; scoresI.hasNext(); k++) {
			final Map.Entry<String, Integer> score = scoresI.next();
			
			final NBAPlayerScore playerScore = new NBAPlayerScore();
			playerScore.setName(score.getKey());
			playerScore.setScore(score.getValue());
			
			result[k] = playerScore;
		}
		
		return result;
	}
	
	public static Map.Entry<String, Integer> generatePlayerScore(String player, int score) {
		return new AbstractMap.SimpleEntry<String, Integer>(player, score);
	}

	public static NBAPlayerScore findPlayerScoreWithNoScore(NBAMatch resultEntry) {
		for (NBAPlayerScore score : resultEntry.getHomeTeam().getPlayerScores()) {
			if (score.getScore() == 0) {
				return score;
			}
		}

		for (NBAPlayerScore score : resultEntry.getVisitorTeam().getPlayerScores()) {
			if (score.getScore() == 0) {
				return score;
			}
		}
		
		return null;
	}
	
	public static <T> T fromJSON(String json, Class<T> resultClass) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
			return mapper.readValue(json, resultClass);
		} catch (JsonMappingException e) {
			assertFailException("JSON Mapping Error: Failed to convert json to \"" + resultClass + "\"!", e);
		} catch (JsonProcessingException e) {
			assertFailException("JSON Processing Error: Failed to convert json to \"" + resultClass + "\"!", e);
		}
		
		return null;
	}
	
	public static <T> List<T> fromJSONList(String json, Class<T> resultClass) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
			final Object readVal = mapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, resultClass));
			if (readVal != null && List.class.isAssignableFrom(readVal.getClass())) {
				List<T> resultList = new ArrayList<T>();
				
				List<?> readValList = (List<?>) readVal;
				for (Object o : readValList) {
					resultList.add(resultClass.cast(o));
				}
				
				return resultList;
						
			}
		} catch (JsonMappingException e) {
			assertFailException("JSON Mapping Error: Failed to convert json to \"" + resultClass + "\"!", e);
		} catch (JsonProcessingException e) {
			assertFailException("JSON Processing Error: Failed to convert json to \"" + resultClass + "\"!", e);
		}
		
		return null;
	}
	
	public static void assertFailException(String message, Exception e) {
		final StringWriter stringWriter = new StringWriter();
		final PrintWriter sw = new PrintWriter(stringWriter);
		try {
			e.printStackTrace(sw);
			Assert.fail(message + " Details: " + stringWriter.toString());
		} finally {
			sw.close();
		}
	}

	public static ResultActions expectDefaultHeaders(ResultActions perform) throws Exception {
		return perform.andExpect(header().exists(Constants.HTTP_HEADER_EVENT_ID))
				.andExpect(header().exists(Constants.HTTP_HEADER_RESULT_CODE))
				.andExpect(header().exists(Constants.HTTP_HEADER_RESULT_DESC));
	}
}
