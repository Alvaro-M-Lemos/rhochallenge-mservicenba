package rho.challenge.nba;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import rho.challenge.nba.constants.Constants;
import rho.challenge.nba.controller.response.Comment;
import rho.challenge.nba.controller.response.Nbaresponse;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.repository.NBAMatchCommentRepository;
import rho.challenge.nba.repository.NBAMatchRepository;
import rho.challenge.nba.utils.TestUtils;

// test only the REST
@RunWith(SpringRunner.class)
@SpringBootTest(
	webEnvironment = SpringBootTest.WebEnvironment.MOCK,
	classes = RhoChallengeMServiceNbaApplication.class
)
@AutoConfigureMockMvc
@TestPropertySource(
  locations = "classpath:application-integrationtest.properties")
class RhoChallengeMServiceNbaApplicationTests {
	private static final String URL_API_NBA_MATCHES = "/api/v1/nba/matches/";
	
	private static final String API_QUERY_PARAM_DATE = "date";

	@Autowired
    private MockMvc mvc;
	
	@Autowired
	public NBAMatchRepository matchRep;
	
	@Autowired
	public NBAMatchCommentRepository matchCommentRep;
	
	@BeforeEach
	public void setUp() {
		matchRep.deleteAll();
		matchCommentRep.deleteAll();
	}

	// Test Get Matches by Date
	/**
	 * Given I got no NBAMatches<br>
	 * And I get all NBAMatches for a given Date<br>
	 * And status is 200<br>
	 * Then result is a filled list with NBAMatches for the specified date.
	 */
	@Test
	public void getAllNBAMatchesForDate()  throws Exception {
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		get(URL_API_NBA_MATCHES)
	    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
	    			.contentType(MediaType.APPLICATION_JSON)
	    	))
	      	.andExpect(status().isOk())
	      	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	      	.andExpect(jsonPath("$[*].gameDate", everyItem(equalToObject(Constants.DEFAULT_QUERY_DATE_VAL))));
	}
	
	/**
	 * Given I got NBAMatches for date {@link Constants#DEFAULT_QUERY_DATE_VAL}<br>
	 * And I query for the first match in the date<br>
	 * And status is 200<br>
	 * Then I got match
	 */
	@Test
	public void getSingleNBAMatch() throws Exception {
		// do a date query that will give us matches
		final String content = TestUtils.expectDefaultHeaders(
				mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
				)
			)
	      	.andExpect(status().isOk())
	      	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	      	.andExpect(jsonPath("$[*].gameDate", everyItem(equalToObject(Constants.DEFAULT_QUERY_DATE_VAL))))
	      	.andReturn().getResponse().getContentAsString();
		
		final List<Nbaresponse> result = TestUtils.fromJSONList(content, Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", result);
	    Assert.assertFalse("NBA response list is empty!", result.isEmpty());
	    
	    // get first match
	    final Nbaresponse nbaResp = result.get(0);
	    final String gameId = nbaResp.getGameId();
		    
	    // fetch single and expect same id and date.
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		get(URL_API_NBA_MATCHES + gameId)
	    			.contentType(MediaType.APPLICATION_JSON)
	    	))
	      	.andExpect(status().isOk())
	      	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	      	.andExpect(jsonPath("$.gameId", is(nbaResp.getGameId())))
	      	.andExpect(jsonPath("$.gameDate", is(nbaResp.getGameDate())));
	}
	
	// Test Add Comment
	/**
	 * Given I got NBAMatches<br>
	 * And I add a comment<br>
	 * And status is 200<br>
	 * Then result NBAMatch contains comment.
	 */
	@Test
	public void addCommentToNBAMatch() throws Exception {
		final String commentToAdd = "Test Comment";
		
		// Get matches for date
	    MvcResult result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$[*].gameDate", everyItem(equalToObject(Constants.DEFAULT_QUERY_DATE_VAL)))).andReturn();
	    
	    List<Nbaresponse> responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    final String gameId = responseList.get(0).getGameId();
	    // POST comment
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		post(URL_API_NBA_MATCHES + gameId + "/comments")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(commentToAdd)
	    	))
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$.gameId", is(gameId)))
	    	.andExpect(jsonPath("$.comment", is(commentToAdd)));
	    
	    // Get matches for the same date, one must have the comment that we added
	    result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();
	    
	    // check that the comment exists on the game that we added it to.
	    responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    assertCommentFromGameExists(gameId, commentToAdd, null, responseList);
	}
	
	// Test update comment
	/**
	 * Given I got NBAMatches<br>
	 * And I add a comment<br>
	 * And I update added comment
	 * And status is 200<br>
	 * Then result NBAMatch contains updated comment.
	 */
	@Test
	public void updateCommentInNBAMatch() throws Exception {
		final String commentToAdd = "Test Comment";
		final String commentToUpd = "Updated Test Comment";
		
		// Get matches for date
	    MvcResult result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$[*].gameDate", everyItem(equalToObject(Constants.DEFAULT_QUERY_DATE_VAL)))).andReturn();
	    
	    List<Nbaresponse> responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    final String gameId = responseList.get(0).getGameId();
	    // POST comment
	    result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		post(URL_API_NBA_MATCHES + gameId + "/comments")
		    			.contentType(MediaType.APPLICATION_JSON)
		    			.content(commentToAdd)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$.gameId", is(gameId)))
	    	.andExpect(jsonPath("$.comment", is(commentToAdd))).andReturn();
	    
	    // Update comment
	    final NBAMatchComment addedComment = TestUtils.fromJSON(result.getResponse().getContentAsString(), NBAMatchComment.class);
	    final String commentId = addedComment.getId();
	    
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		put(URL_API_NBA_MATCHES + gameId + "/comments/" + commentId)
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(commentToUpd)
	    	))
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$.gameId", is(gameId)))
	    	.andExpect(jsonPath("$.comment", is(commentToUpd)));
	    
	    // Get matches for the same date, one must have the comment that we updated
	    result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();
	    
	    // check that the comment exists on the game that we added it to.
	    responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    assertCommentFromGameExists(gameId, commentToUpd, null, responseList);
	}
	
	// Test delete
	/**
	 * Given I got NBAMatches<br>
	 * And I add a comment to a NBAMatch<br>
	 * And I delete added comment
	 * And status is 200<br>
	 * Then result NBAMatch does not contain comment.
	 */
	@Test
	public void deleteCommentInNBAMatch() throws Exception {
		final String commentToAdd = "Test Comment";
		
		// Get matches for date
	    MvcResult result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$[*].gameDate", everyItem(equalToObject(Constants.DEFAULT_QUERY_DATE_VAL)))).andReturn();
	    
	    List<Nbaresponse> responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    final String gameId = responseList.get(0).getGameId();
	    // POST comment
	    final MvcResult addedCommentResponse = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		post(URL_API_NBA_MATCHES + gameId + "/comments")
		    			.contentType(MediaType.APPLICATION_JSON)
		    			.content(commentToAdd)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$.gameId", is(gameId)))
	    	.andExpect(jsonPath("$.comment", is(commentToAdd))).andReturn();
	    
	    final NBAMatchComment addedComment = TestUtils.fromJSON(addedCommentResponse.getResponse().getContentAsString(), NBAMatchComment.class);
	    final String commentId = addedComment.getId();
	    
	    // Get matches for the same date, one must have the comment that we added
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		get(URL_API_NBA_MATCHES)
	    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
	    			.contentType(MediaType.APPLICATION_JSON)
	    	))
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$[*].comments[*].id", hasItem(commentId)));
	    
	    // DELETE comment
	    TestUtils.expectDefaultHeaders(mvc.perform(
	    		delete(URL_API_NBA_MATCHES + gameId + "/comments/" + commentId)
	    			.contentType(MediaType.APPLICATION_JSON)
	    	))
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	.andExpect(jsonPath("$.id", is(commentId)));
	    
	    // get matches for the same date
	    result = TestUtils.expectDefaultHeaders(
	    		mvc.perform(
		    		get(URL_API_NBA_MATCHES)
		    			.queryParam(API_QUERY_PARAM_DATE, Constants.DEFAULT_QUERY_DATE_VAL)
		    			.contentType(MediaType.APPLICATION_JSON)
	    		)
	    	)
	    	.andExpect(status().isOk())
	    	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();
	    
	    // check that the comment exists on the game that we added it to.
	    responseList = TestUtils.fromJSONList(result.getResponse().getContentAsString(), Nbaresponse.class);
	    Assert.assertNotNull("NBA response list is null!", responseList);
	    Assert.assertFalse("NBA response list is empty!", responseList.isEmpty());
	    
	    final Nbaresponse match = TestUtils.findNBAMatchWithId(gameId, responseList);
	    Assert.assertNotNull("No game with id " + gameId + " was found!", match);
	    
	    for (Comment comment : match.getComments()) {
	    	Assert.assertNotEquals("Found comment with id \"" + commentId + "\"!", commentId, comment.getId());
	    }
	}

	private Comment assertCommentFromGameExists(final String gameId, final String commentText,
			final String commentId, List<Nbaresponse> responseList) {
		Comment result = null;
	    for (Nbaresponse nbaResponse : responseList) {
	    	if (nbaResponse.getGameId().equals(gameId)) {
	    		Assert.assertNotNull("Null comments on game with id \"" + gameId + "\"!", nbaResponse.getComments());
	    		Assert.assertFalse("No comments on game with id \"" + gameId + "\"!", nbaResponse.getComments().isEmpty());
	    		
	    		for (Comment comment : nbaResponse.getComments()) {
	    			if (comment.getComment().equals(commentText) 
	    					|| comment.getId().equals(commentId)) {
	    				result = comment;
	    				break;
	    			}
	    		}
	    		
	    		break;
	    	}
	    }
	    
	    Assert.assertNotNull("Comment \"" + commentText +"\" was not found in game with id \"" + gameId + "\"!", result);
	    
	    return result;
	}

}
