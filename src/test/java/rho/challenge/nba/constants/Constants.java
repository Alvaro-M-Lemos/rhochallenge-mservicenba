package rho.challenge.nba.constants;

import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.utils.Context;
import rho.challenge.nba.utils.TestUtils;
import rho.challenge.nba.utils.Utils;

public class Constants {
	private static final Logger LOG = LoggerFactory.getLogger(Constants.class);
	
	public static final String HTTP_HEADER_EVENT_ID = "NBA-Serv-EventId";
	public static final String HTTP_HEADER_RESULT_CODE = "NBA-Serv-ResultCode";
	public static final String HTTP_HEADER_RESULT_DESC = "NBA-Serv-ResultDescription";
	
	public static final int DEFAULT_QUERY_PAGE = 1;
	public static final int DEFAULT_QUERY_PAGE_SIZE = 25;
	public static final String DEFAULT_QUERY_DATE_VAL = "2019-01-01";
	public static final Date DEFAULT_QUERY_DATE = generateDefaultQueryDate();
	
	public static final NBAMatch DEFAULT_MATCH = TestUtils.generateSampleMatch(
			"TeamA", 45, 
			"TeamB", 35,
			TestUtils.generateScores(Arrays.asList(
					TestUtils.generatePlayerScore("Player1", 20),
					TestUtils.generatePlayerScore("Player2", 25),
					TestUtils.generatePlayerScore("Player4", 0)
			)),
			TestUtils.generateScores(Arrays.asList(
					TestUtils.generatePlayerScore("Player5", 15),
					TestUtils.generatePlayerScore("Player6", 15),
					TestUtils.generatePlayerScore("Player7", 5)
			))
	);
	
	public static final String DEFAULT_ADD_COMMENT = "Test Comment";
	public static final String DEFAULT_UPDATE_COMMENT = "Updated Test Comment";
	
	public static final Context DEFAULT_CTX = new Context();
	
	private static Date generateDefaultQueryDate() {
		try {
			return Utils.fromTextualDate(DEFAULT_QUERY_DATE_VAL);
		} catch (NBAServiceException e) {
			LOG.error("An error occured when attempting to generate default "
					+ "query date value {} as a date java type.", DEFAULT_QUERY_DATE_VAL);
		}
		return null;
	}
	
}
