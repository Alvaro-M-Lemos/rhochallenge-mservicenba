package rho.challenge.nba.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;

import rho.challenge.nba.constant.Constants;
import rho.challenge.nba.constant.ResultCode;
import rho.challenge.nba.controller.response.Comment;
import rho.challenge.nba.controller.response.Nbaresponse;
import rho.challenge.nba.controller.response.PlayerScore;
import rho.challenge.nba.controller.response.Score;
import rho.challenge.nba.controller.response.Team;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.model.NBAMatchTeam;
import rho.challenge.nba.model.NBAPlayerScore;

public class RESTUtils {
	private static final Logger LOG = LoggerFactory.getLogger(RESTUtils.class); 
	
	public static <T> ResponseEntity<T> generateErrorResponse(Exception e) {
		return generateErrorResponse(null, e, ResultCode.UNCHECKED_ERROR);
	}
	
	public static <T> ResponseEntity<T> generateErrorResponse(String eventId, Exception e) {
		return generateErrorResponse(eventId, e, ResultCode.UNCHECKED_ERROR);
	}

	public static <T> ResponseEntity<T> generateErrorResponse(NBAServiceException e) {
		return generateErrorResponse(null, e);
	}
	
	public static <T> ResponseEntity<T> generateErrorResponse(String eventId, NBAServiceException e) {
		if (eventId != null) {
			e.setEventId(eventId);
		}
		
		return generateResponse(e.getEventId(), e.getHttpStatus(), e.getSystemCode(), e.getMessage());
	}
	
	public static <T> ResponseEntity<T> generateErrorResponse(String eventId, Exception e, ResultCode resultCode) {
		return generateResponse(eventId, resultCode.getHttpStatus(), resultCode.getSystemCode(), resultCode.getDescription() + " [" + e.getMessage() + "]");
	}
	
	public static <T> ResponseEntity<T> generateResponse(String eventId, HttpStatus status, String systemCode, String message) {
		return generateResponse(eventId, status, systemCode, message, null);
	}
	
	public static <T> ResponseEntity<T> generateResponse(String eventId, HttpStatus status, String systemCode, String message, T body) {
		final BodyBuilder bodyBuilder = ResponseEntity.status(status)
				.header(Constants.HTTP_HEADER_RESULT_CODE, systemCode)
				.header(Constants.HTTP_HEADER_RESULT_DESC, message);
		
		if (eventId != null) {
			bodyBuilder.header(Constants.HTTP_HEADER_EVENT_ID, eventId);
		}
		
		return bodyBuilder.body(body);
	}
	
	public static Nbaresponse toNBAResponse(Context ctx, NBAMatch match, List<NBAMatchComment> comments) {
		if (match == null) {
			return null;
		}
		
		LOG.debug("[{}] Converting NBAMatch {} to a NBAResponse...", ctx.getEventId(), match.getGameId());
		final Nbaresponse response = new Nbaresponse();
		
		response.setGameId(match.getGameId());
		response.setGameDate(Utils.toTextualDate(match.getGameDate()));

		LOG.debug("[{}] Filling NBAMatch {} team info...", ctx.getEventId(), match.getGameId());
		response.setHomeTeam(fromNBATeam(match.getHomeTeam()));
		response.setAwayTeam(fromNBATeam(match.getVisitorTeam()));
		
		LOG.debug("[{}] Filling NBAMatch {} comment info...", ctx.getEventId(), match.getGameId());
		if (comments != null) {
			for (NBAMatchComment comment : comments) {
				Comment respComment = new Comment();
				respComment.setId(comment.getId());
				respComment.setComment(comment.getComment());
				respComment.setDate(Utils.toTextualDate(comment.getDate(), Constants.DATE_FORMAT_RFC3339NANO));
				response.getComments().add(respComment);
			}
		}
		
		return response;
	}

	private static Team fromNBATeam(NBAMatchTeam team) {
		if (team == null) {
			return null;
		}
		
		final Team result = new Team();
		
		result.setTeamName(team.getName());
		result.setScore(new Score());
		result.getScore().setTotalScore(team.getTotalScore());
		for (NBAPlayerScore playerScore : team.getPlayerScores()) {
			final PlayerScore score = new PlayerScore();
			score.setPlayerName(playerScore.getName());
			score.setPlayerScore(playerScore.getScore());
			result.getScore().getPlayerScores().add(score);
		}
		
		return result;
	}

	public static <T> ResponseEntity<T> generateOKResponse(String eventId, T body) {
		return generateResponse(
				eventId,
				ResultCode.OK.getHttpStatus(), 
				ResultCode.OK.getSystemCode(), 
				ResultCode.OK.getDescription(), 
				body
		);
	}

	public static Set<String> aggregateGameIdsFromMatches(List<NBAMatch> matches) {
		final HashSet<String> result = new HashSet<>();
		
		for (NBAMatch match : matches) {
			result.add(match.getGameId());
		}
		
		return result;
	}
}
