package rho.challenge.nba.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import rho.challenge.nba.constant.Constants;
import rho.challenge.nba.constant.ResultCode;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.model.NBAMatch;

public class Utils {
	private static final Logger LOG = LoggerFactory.getLogger(Utils.class); 
	
	public static Date fromTextualDate(String date) throws NBAServiceException {
		LOG.trace("Converting textual date \"{}\" to java date...", date);
		try {
			return generateDefaultDateFormatter().parse(date);
		} catch (ParseException e) {
			throw new NBAServiceException(null, 
					ResultCode.INVALID_INPUT_DATA, 
					"The format must be " + Constants.DEFAULT_DATE_FORMAT, 
					e
			);
		}
	}

	public static String toTextualDate(Date gameDate) {
		LOG.trace("Converting java date \"{}\" to textual date...", gameDate);
		if (gameDate == null) {
			return null;
		}
		
		return generateDefaultDateFormatter().format(gameDate);
	}

	public static String toTextualDate(Date gameDate, String dateFormat) {
		LOG.trace("Converting java date \"{}\" to textual date with date format {}...", gameDate, dateFormat);
		if (gameDate == null) {
			return null;
		}
		
		final SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setTimeZone(TimeZone.getTimeZone(ZoneId.of(Constants.DEFAULT_DATE_TIME_ZONE)));
		
		return format.format(gameDate);
	}
	
	public static Date fromTextualDate(String date, String dateFormat) throws NBAServiceException {
		LOG.trace("Converting textual date \"{}\" to java date expecting date format {}...", date, dateFormat);
		try {
			final SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			format.setTimeZone(TimeZone.getTimeZone(ZoneId.of(Constants.DEFAULT_DATE_TIME_ZONE)));
			
			return format.parse(date);
		} catch (ParseException e) {
			throw new NBAServiceException(null, 
					ResultCode.INVALID_INPUT_DATA, 
					"The format must be " + dateFormat, 
					e
			);
		}
	}
	
	private static SimpleDateFormat generateDefaultDateFormatter() {
		LOG.trace("Constructing SimpleDateFormat with date format {} and time zone {}", 
				Constants.DEFAULT_DATE_FORMAT, Constants.DEFAULT_DATE_TIME_ZONE);
		final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone(ZoneId.of(Constants.DEFAULT_DATE_TIME_ZONE)));
		return dateFormat;
	}

	public static List<NBAMatch> fromPageToList(Page<NBAMatch> findByGameDate) {
		if (findByGameDate == null) {
			return Collections.emptyList();
		}
		
		return findByGameDate.getContent();
	}
	
}
