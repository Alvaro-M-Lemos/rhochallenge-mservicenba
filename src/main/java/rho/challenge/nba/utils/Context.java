package rho.challenge.nba.utils;

import java.util.UUID;

public class Context {
	private String eventId;
	
	public Context() {
		this.eventId = UUID.randomUUID().toString();
	}
	
	public String getEventId() {
		return this.eventId;
	}
}
