package rho.challenge.nba.utils;

import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilder;

import reactor.core.publisher.Mono;
import rho.challenge.nba.constant.Constants;
import rho.challenge.nba.constant.ResultCode;
import rho.challenge.nba.exception.NBAServiceException;

public class HTTPUtils {
	private static final Logger LOG = LoggerFactory.getLogger(HTTPUtils.class);
	
	public static <T> ResponseEntity<T> doGet(
			Context ctx,
			String url, 
			Map<String, String> uriVariables,
			Map<String, String> queryParams, 
			MultiValueMap<String, String> requestHeaders,
			int timeout,
			Class<T> responseType) throws NBAServiceException {
		
		try {
			LOG.debug("[{}] Attempting to perform HTTP GET to url {} with uri vars {}, "
					+ "query params {} and request headers {}, timing out after {} "
					+ "seconds expecting {} as type of body...",
					ctx.getEventId(), url, uriVariables, queryParams, requestHeaders, timeout, responseType);
			
			final UriBuilder builder = new DefaultUriBuilderFactory(url).builder();
			
			if (queryParams != null) {
				for (Map.Entry<String, String> queryParam : queryParams.entrySet()) {
					builder.queryParam(queryParam.getKey(), queryParam.getValue());
				}
			}
			
			final URI uri = uriVariables != null ? builder.build(uriVariables) : builder.build();
			final RequestHeadersUriSpec<?> request = WebClient.create().get();
			
			if (requestHeaders != null) {
				for (Map.Entry<String, List<String>> headerEntry : requestHeaders.entrySet()) {
					String[] headerValues = new String[headerEntry.getValue().size()];
					headerEntry.getValue().toArray(headerValues);
					
					request.header(headerEntry.getKey(), headerValues);
				}
			}
			
			final Mono<ResponseEntity<T>> response = request.uri(uri)
					.retrieve().toEntity(responseType);
			
			LOG.debug("[{}] Waiting for response...", ctx.getEventId());
			try {
				return waitForResponse(timeout, response);
			} catch (RuntimeException e) {
				throw new NBAServiceException(
						ctx.getEventId(), 
						ResultCode.HTTP_REQUEST_FAILED, 
						"When attempting to do a GET request to \"" + url + "\"",
						e
				);
			}
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to do a GET request to \"" + url + "\"",
					e
			);
		}
	}

	private static <T> ResponseEntity<T> waitForResponse(int timeout, final Mono<ResponseEntity<T>> response) {
		ResponseEntity<T> httpResponse;
		if (timeout == 0) {
			httpResponse = response.block();
		} else {
			if (timeout < 0) {
				timeout = Constants.DEFAULT_HTTP_TIMEOUT_DELAY_SECONDS;
			}
			
			httpResponse = response.block(Duration.of(timeout, ChronoUnit.SECONDS));
		}
		return httpResponse;
	}
	
}
