package rho.challenge.nba.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rho.challenge.nba.constant.ResultCode;
import rho.challenge.nba.exception.NBAServiceException;

public class JSONUtils {
	public static final <T> String toJSON(T obj) throws NBAServiceException {
		final ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new NBAServiceException(
					null, ResultCode.DATA_CONVERSION_FAILED, 
					"when attempting to convert from JavaClass to JSON", 
					e
			);
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					null, ResultCode.DATA_CONVERSION_FAILED, 
					"when attempting to convert from JavaClass to JSON", 
					new NBAServiceException(null, ResultCode.UNCHECKED_ERROR, e) 
			);
		}
	}
}
