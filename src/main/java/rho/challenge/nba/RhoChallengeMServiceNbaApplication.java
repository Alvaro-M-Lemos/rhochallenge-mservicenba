package rho.challenge.nba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class RhoChallengeMServiceNbaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RhoChallengeMServiceNbaApplication.class, args);
	}

}
