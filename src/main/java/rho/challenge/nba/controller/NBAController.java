package rho.challenge.nba.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rho.challenge.nba.controller.response.Nbaresponse;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.service.NBAMatchesService;
import rho.challenge.nba.utils.Context;
import rho.challenge.nba.utils.JSONUtils;
import rho.challenge.nba.utils.RESTUtils;

@RestController
@RequestMapping("api/v1/nba")
public class NBAController {
	private static final Logger LOG = LoggerFactory.getLogger(NBAController.class); 

	@Autowired
	private NBAMatchesService matchesService;
	
	@GetMapping
	public ResponseEntity<String> heartbeat() {
		return ResponseEntity.status(HttpStatus.OK).body("Up and running.");
	}
	
	@GetMapping(path = "/matches")
	public ResponseEntity<List<Nbaresponse>> listAllMatches(
				@RequestParam(name = "date", required = true) String date,
				@RequestParam(name = "page", required = false, defaultValue = "-1") int page,
				@RequestParam(name = "pageSize", required = false, defaultValue = "-1") int pageSize
			) {
		final Context ctx = new Context();
		LOG.info("[{}] Received request: \"listAllMatches\" with input date \"{}\"", 
				ctx.getEventId(), date
		);
		
		try {
			final List<Nbaresponse> body = new LinkedList<Nbaresponse>();
			final List<NBAMatch> matches = matchesService.getAllNBAMatches(ctx, date, page, pageSize);
			final Map<String, List<NBAMatchComment>> comments = matchesService.getAllNBAMatchComments(
					ctx, RESTUtils.aggregateGameIdsFromMatches(matches)
			);
			
			LOG.debug("[{}] Building response...", ctx.getEventId());
			for (NBAMatch match : matches) {
				body.add(RESTUtils.toNBAResponse(ctx, match, comments.get(match.getGameId())));
			}

			LOG.info("[{}] Request \"listAllMatches\" with input date \"{}\" was successful with {} matches to return!", 
					ctx.getEventId(), date, body.size()
			);
			return RESTUtils.generateOKResponse(ctx.getEventId(), body);
		} catch (NBAServiceException e) {
			LOG.error("[{}] listAllMatches request failed! Details: {}", ctx.getEventId(), e.toString());
			
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		} catch (RuntimeException e) {
			LOG.error("[{}] listAllMatches request failed! Runtime Error occurred! Details: {}", ctx.getEventId(), ctx.getEventId(), e);
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		}
	}
	
	@GetMapping(path = "/matches/{matchId}")
	public ResponseEntity<Nbaresponse> getSingleMatch(@PathVariable String matchId) {
		final Context ctx = new Context();
		LOG.info("[{}] Received request: \"getSingleMatch\" with input match id \"{}\"", 
				ctx.getEventId(), matchId
		);
		
		try {
			final NBAMatch match = matchesService.getSingleNBAMatch(ctx, matchId);
			
			final List<NBAMatchComment> comments = matchesService.getAllNBAMatchComments(
					ctx, match.getGameId()
			);
			
			LOG.debug("[{}] Building response...", ctx.getEventId());
			final Nbaresponse body = RESTUtils.toNBAResponse(ctx, match, comments);

			LOG.info("[{}] Request \"getSingleMatch\" with input match id \"{}\" was successful!", 
					ctx.getEventId(), matchId
			);
			return RESTUtils.generateOKResponse(ctx.getEventId(), body);
		} catch (NBAServiceException e) {
			LOG.error("[{}] getSingleMatch request failed! Details: {}", ctx.getEventId(), e.toString());
			
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		} catch (RuntimeException e) {
			LOG.error("[{}] getSingleMatch request failed! Runtime Error occurred! Details: {}", ctx.getEventId(), ctx.getEventId(), e);
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		}
	}
	
	@PostMapping(path = "/matches/{gameId}/comments")
	public ResponseEntity<NBAMatchComment> addCommentToMatch(@PathVariable String gameId, @RequestBody String comment) {
		final Context ctx = new Context();
		try {
			if (LOG.isInfoEnabled()) {
				LOG.info("[{}] Received request: \"addCommentToMatch\" with input match \"{}\"", 
						ctx.getEventId(), JSONUtils.toJSON(comment)
				);
			}
			
			final NBAMatchComment match = this.matchesService.addNBAMatchComment(ctx, gameId, comment);
			
			return RESTUtils.generateOKResponse(
					ctx.getEventId(), 
					match
			);
		} catch (NBAServiceException e) {
			LOG.error("[{}] addCommentToMatch request failed! Details: {}", ctx.getEventId(), e.toString());
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		} catch (RuntimeException e) {
			LOG.error("[{}] addCommentToMatch request failed! Runtime Error occurred! Details: {}", ctx.getEventId(), e);
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		}
	}
	
	@PutMapping(path = "/matches/{gameId}/comments/{commentId}")
	public ResponseEntity<NBAMatchComment> updateMatchComment(@PathVariable String gameId, @PathVariable String commentId, 
			@RequestBody String comment) {
		final Context ctx = new Context();
		try {
			if (LOG.isInfoEnabled()) {
				LOG.info("[{}] Received request: \"updateMatchComment\" with input match \"{}\"", 
						ctx.getEventId(), JSONUtils.toJSON(comment)
				);
			}
			
			final NBAMatchComment match = this.matchesService.updateNBAMatchComment(ctx, gameId, commentId, comment);
			
			return RESTUtils.generateOKResponse(
					ctx.getEventId(), 
					match
			);
		} catch (NBAServiceException e) {
			LOG.error("[{}] updateMatchComment request failed! Details: {}", ctx.getEventId(), e.toString());
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		} catch (RuntimeException e) {
			LOG.error("[{}] updateMatchComment request failed! Runtime Error occurred! Details: {}", ctx.getEventId(), e);
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		}
	}
	
	@DeleteMapping(path = "/matches/{gameId}/comments/{commentId}")
	public ResponseEntity<NBAMatchComment> deleteCommentFromMatch(@PathVariable String gameId, @PathVariable String commentId) {
		final Context ctx = new Context();
		try {
			LOG.info("[{}] Received request: \"deleteCommentFromMatch\" with game id {} and comment id {}", 
					ctx.getEventId(), gameId, commentId
			);
			
			final NBAMatchComment match = this.matchesService.deleteNBAMatchComment(ctx, gameId, commentId);
			
			return RESTUtils.generateOKResponse(
					ctx.getEventId(), 
					match
			);
		} catch (NBAServiceException e) {
			LOG.error("[{}] deleteCommentFromMatch request failed! Details: {}", ctx.getEventId(), e.toString());
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		} catch (RuntimeException e) {
			LOG.error("[{}] deleteCommentFromMatch request failed! Runtime Error occurred! Details: {}", ctx.getEventId(), e);
			return RESTUtils.generateErrorResponse(ctx.getEventId(), e);
		}
	}
}
