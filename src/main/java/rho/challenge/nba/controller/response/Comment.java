package rho.challenge.nba.controller.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Comment {
    private String id;
    private String comment;
    private String date;
}
