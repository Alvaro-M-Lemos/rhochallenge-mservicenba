package rho.challenge.nba.controller.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Score {
    private Integer totalScore;
    private List<PlayerScore> playerScores = new ArrayList<PlayerScore>();
}
