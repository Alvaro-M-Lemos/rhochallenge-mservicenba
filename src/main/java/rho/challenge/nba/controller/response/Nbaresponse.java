package rho.challenge.nba.controller.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Nbaresponse {

    private String gameId;
    private String gameDate;
    private Team homeTeam;
    private Team awayTeam;
    private List<Comment> comments = new ArrayList<Comment>();

}
