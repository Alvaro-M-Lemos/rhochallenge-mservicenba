package rho.challenge.nba.controller.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class PlayerScore {

    private String playerName;
    private Integer playerScore;
}
