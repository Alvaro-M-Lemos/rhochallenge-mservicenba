package rho.challenge.nba.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import rho.challenge.nba.model.NBAMatch;

public interface NBAMatchRepository extends MongoRepository<NBAMatch, String> {
	public List<NBAMatch> findByGameDate(Date date);
	public Page<NBAMatch> findByGameDate(Date date, Pageable pageable);
	
	public boolean existsByGameDate(Date date);
}
