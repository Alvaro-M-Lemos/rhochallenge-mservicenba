package rho.challenge.nba.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import rho.challenge.nba.model.NBAMatchComment;

public interface NBAMatchCommentRepository extends MongoRepository<NBAMatchComment, String> {
	public List<NBAMatchComment> findAllByGameIdIn(Iterable<String> ids, Sort sortOptions);
}
