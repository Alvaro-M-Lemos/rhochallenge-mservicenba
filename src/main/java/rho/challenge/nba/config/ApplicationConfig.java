package rho.challenge.nba.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.Immutable;

@Configuration
@ConfigurationProperties(prefix = "app")
@Immutable
public class ApplicationConfig {
	private int httpRequestTimeout;
	private String nbaapi_key;

	public int getHttpRequestTimeout() {
		return httpRequestTimeout;
	}

	public void setHttpRequestTimeout(int httpRequestTimeout) {
		this.httpRequestTimeout = httpRequestTimeout;
	}

	public String getNbaapi_key() {
		return nbaapi_key;
	}

	public void setNbaapi_key(String nbaapi_key) {
		this.nbaapi_key = nbaapi_key;
	}
}
