package rho.challenge.nba.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings.Builder;

@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {

	@Autowired
	MongoProperties properties;
	
    @Bean
    MongoTransactionManager transactionManager(MongoDatabaseFactory dbFactory) {
        return new MongoTransactionManager(dbFactory);
    }
    
    @Override
    protected void configureClientSettings(Builder builder) {
    	super.configureClientSettings(builder);
    	
    	System.out.println(properties.getUri());
    	
    	builder.applyConnectionString(new ConnectionString(properties.getUri()));
    }
    
	@Override
	protected boolean autoIndexCreation() {
		if (properties.isAutoIndexCreation() != null) {
			return properties.isAutoIndexCreation();
		}
		
		return super.autoIndexCreation();
	}
	

	@Override
	protected String getDatabaseName() {
		return properties.getDatabase();
	}

}
