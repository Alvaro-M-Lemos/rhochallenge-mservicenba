package rho.challenge.nba.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.Immutable;

@Configuration
@ConfigurationProperties(prefix = "cache")
@Immutable
public class CacheConfig {
	private int timeToLive;
	
	public void setTimeToLive(int timeToLive) {
		this.timeToLive = timeToLive;
	}
	
	public int getTimeToLive() {
		return this.timeToLive;
	}
}
