package rho.challenge.nba.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rho.challenge.nba.config.ApplicationConfig;
import rho.challenge.nba.config.CacheConfig;
import rho.challenge.nba.constant.Constants;
import rho.challenge.nba.constant.ResultCode;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.http.api.FreeNBAAPI;
import rho.challenge.nba.http.api.response.Data;
import rho.challenge.nba.http.api.response.Metadata;
import rho.challenge.nba.http.api.response.NBAAPIResponse;
import rho.challenge.nba.model.NBAMatch;
import rho.challenge.nba.model.NBAMatchComment;
import rho.challenge.nba.model.NBAMatchTeam;
import rho.challenge.nba.model.NBAPlayerScore;
import rho.challenge.nba.repository.NBAMatchCommentRepository;
import rho.challenge.nba.repository.NBAMatchRepository;
import rho.challenge.nba.utils.Context;
import rho.challenge.nba.utils.JSONUtils;
import rho.challenge.nba.utils.Utils;

@Service
public class NBAMatchesService {
	private static final Logger LOG = LoggerFactory.getLogger(NBAMatchesService.class); 

	private final ApplicationConfig appConfig;
	
	@Autowired
	private CacheConfig cacheConfig;
	@Autowired
	private NBAMatchRepository matchRepository;
	@Autowired
	private NBAMatchCommentRepository matchCommentRepository;
	
	private final FreeNBAAPI freeNBAAPI;
	
	@Autowired
	public NBAMatchesService(final ApplicationConfig appConfig) {
		this.appConfig = appConfig;
		this.freeNBAAPI = new FreeNBAAPI(this.appConfig);
	}
	

	@Transactional
	public NBAMatch getSingleNBAMatch(Context ctx, final String gameId) throws NBAServiceException {
		try {
			LOG.info("[{}] Fetching single NBAMatch for with gameId {}...", ctx.getEventId(), gameId);

			boolean processCache = true;
			Optional<NBAMatch> match = matchRepository.findById(gameId);

			if (!match.isPresent()) {
				LOG.debug("[{}] No match with game id {} exists currently on BD, fetching from API...", ctx.getEventId(), gameId);
				
				// fetch date so we can cache all games for the date...
				// Because of how getAllNBAMatches function works, the program 
				// will never get the full list of games for a specific date if we don't get it here.
				// (if a game already exists for that date he will assume the API only returned that game)
				final Date dateForGame = this.getDateForGameId(ctx, gameId);
				if (dateForGame != null && fillDBWithDataForDate(ctx, dateForGame)) {
					match = matchRepository.findById(gameId);
					processCache = false;
				}
			}
			
			if (!match.isPresent()) {
				throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, 
						"NBA Match with game id \"" + gameId + "\" does not exist!");
			}
			
			if (processCache) {
				processCacheFor(ctx, match.get());
			}
			
			return processNBAMatchResult(match.get());
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to get NBA match with game id \"" + gameId + "\"", 
					e
			);
		}
	}
	
	@Transactional
	public List<NBAMatch> getAllNBAMatches(Context ctx, final String date, int page, int pageSize) throws NBAServiceException {
		try {
			LOG.info("[{}] Fetching all NBAMatches for date \"{}\" with page {} and pageSize {}...", ctx.getEventId(), date, page, pageSize);
			
			if (date == null || date.isBlank()) {
				throw new NBAServiceException(ctx.getEventId(), ResultCode.INVALID_INPUT_DATA, "Date cannot be null!");
			}
			
			LOG.debug("[{}] Attempting to parse date \"{}\"...", ctx.getEventId(), date);
			final Date parsedDate = Utils.fromTextualDate(date);
			
			final Pageable pageable;
			if (pageSize >= 0 || page >= 0) {
				
				if (pageSize == 0) {
					pageSize = Constants.DEFAULT_PAGE_SIZE;
				}
				
				pageable = PageRequest.of(page, pageSize);
			} else {
				pageable = null;
			}
			
			
			List<NBAMatch> result;
			final Date preparedDate = prepareDate(parsedDate);
			
			LOG.debug("[{}] Attempting to find by GameDate {}", ctx.getEventId(), preparedDate);
			if (pageable != null) {
				result = Utils.fromPageToList(matchRepository.findByGameDate(preparedDate, pageable));
			} else {
				result = matchRepository.findByGameDate(preparedDate);
			}
			
			boolean processCache = true;
			if (result.isEmpty() && !this.matchRepository.existsByGameDate(preparedDate)) {
				LOG.debug("[{}] No results returned, nor exists game data on DB, fetching from API...", ctx.getEventId());
				if (fillDBWithDataForDate(ctx, parsedDate)) {
					processCache = false;
					
					LOG.debug("[{}] Attempting to find by GameDate {} after filling DB with API data...", ctx.getEventId(), preparedDate);
					if (pageable != null) {
						result = Utils.fromPageToList(matchRepository.findByGameDate(preparedDate, pageable));
					} else {
						result = matchRepository.findByGameDate(preparedDate);
					}
				}
			}
			
			// fillDBWithDataForDate already does cache for us.
			if (processCache) {
				processCacheFor(ctx, result);
			}
			
			return processNBAMatchResults(result);
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to list all NBA matches for date \"" + date + "\"", 
					e
			);
		}
	}
	
	public Date getDateForGameId(Context ctx, String gameId) throws NBAServiceException {
		final ResponseEntity<NBAAPIResponse> httpResponse = this.freeNBAAPI.fetchStatsForGameId(
				ctx, 1, 1, gameId
		);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[{}] Got response with status code {} and body {}", ctx.getEventId(), httpResponse.getStatusCode().value(),
					JSONUtils.toJSON(httpResponse.getBody()));
		}
		
		// parse and store data
		if (httpResponse.getStatusCode() != HttpStatus.OK) {
			throw new NBAServiceException(ctx.getEventId(), ResultCode.HTTP_REQUEST_FAILED, 
					"The API request to fetch NBA Match with gameId \"" + gameId 
						+ "\" failed: " + httpResponse.getStatusCode().getReasonPhrase());
		}
		
		if (httpResponse.getBody() == null) {
			throw new NBAServiceException(ctx.getEventId(), ResultCode.HTTP_REQUEST_FAILED, 
					"The API request to fetch NBA Match with gameId \"" + gameId 
						+ "\" failed with a empty/null body! ");
		}
		
		final List<Data> dataList = httpResponse.getBody().getData();
		if (dataList.isEmpty()) {
			LOG.error("[{}] No results for game with id {} were returned from FreeNBA API", ctx.getEventId(), gameId);
			return null;
		}
		
		return Utils.fromTextualDate(dataList.get(0).getGame().getDate(), Constants.DATE_FORMAT_RFC3339NANO);
		
	}

	private boolean fillDBWithDataForDate(Context ctx, Date parsedDate) throws NBAServiceException {
		int page = 1;
		Metadata metadata;
		
		final Map<String, NBAMatch> matchesToSave = new HashMap<String, NBAMatch>();
		do {
			metadata = fillWithDataByDate(ctx, page, Constants.DEFAULT_PAGE_SIZE, parsedDate, matchesToSave);
		} while (page++ < metadata.getTotal_pages());
		
		if (matchesToSave.isEmpty()) {
			return false;
			
		}
		
		// set their timespan date and then save them on DB
		processCacheFor(ctx, matchesToSave.values());
		
		return true;
	}

	private Metadata fillWithDataByDate(Context ctx, int page, int defaultPageSize, Date parsedDate, 
			Map<String, NBAMatch> matchesToSave) throws NBAServiceException {
		final ResponseEntity<NBAAPIResponse> httpResponse = this.freeNBAAPI.fetchStatsForDate(
				ctx, page, Constants.DEFAULT_PAGE_SIZE, parsedDate
		);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[{}] Got response with status code {} and body {}", ctx.getEventId(), httpResponse.getStatusCode().value(),
					JSONUtils.toJSON(httpResponse.getBody()));
		}
		
		// parse and store data
		if (httpResponse.getStatusCode() != HttpStatus.OK) {
			LOG.error("[{}] FreeNBA API request to get matches for date {} failed with status code {}", 
					ctx.getEventId(), httpResponse.getStatusCodeValue());
			throw new NBAServiceException(ctx.getEventId(), ResultCode.HTTP_REQUEST_FAILED, 
					"The API request to fetch all stats for date \"" + parsedDate 
						+ "\" failed: " + httpResponse.getStatusCode().getReasonPhrase());
		}
		
		if (httpResponse.getBody() == null) {
			LOG.error("[{}] FreeNBA API request to get matches for date {} returned no body!", 
					ctx.getEventId(), httpResponse.getStatusCodeValue());
			throw new NBAServiceException(ctx.getEventId(), ResultCode.HTTP_REQUEST_FAILED, 
					"The API request to fetch all stats for date \"" + parsedDate 
						+ "\" failed with a empty/null body! ");
		}
		
		LOG.debug("[{}] Creating NBAMatch from FreeNBA API response...", ctx.getEventId());
		final List<Data> dataList = httpResponse.getBody().getData();
		for (Data data : dataList) {
			final String gameId = String.valueOf(data.getGame().getId());
			
			NBAMatch stored = matchesToSave.get(gameId);
			if (stored == null) {
				//gameId, parsedDate, new NBAMatchTeam(), new NBAMatchTeam()
				stored = new NBAMatch();
				stored.setGameId(gameId);
				stored.setGameDate(parsedDate);
				stored.setHomeTeam(new NBAMatchTeam());
				stored.setVisitorTeam(new NBAMatchTeam());

				stored.getHomeTeam().setTotalScore(data.getGame().getHome_team_score());
				stored.getHomeTeam().setPlayerScores(new LinkedList<NBAPlayerScore>());
				stored.getVisitorTeam().setTotalScore(data.getGame().getVisitor_team_score());
				stored.getVisitorTeam().setPlayerScores(new LinkedList<NBAPlayerScore>());
				
				LOG.debug("[{}] New match with id {}", ctx.getEventId(), gameId);
				matchesToSave.put(gameId, stored);
			}
			
			// add player and team info
			final String playerId = data.getPlayer().getFirst_name() + " " + data.getPlayer().getLast_name();
			final NBAMatchTeam teamToUpdate = data.getGame().getHome_team_id() == data.getTeam().getId() ? 
					stored.getHomeTeam() : stored.getVisitorTeam();
			
			teamToUpdate.setName(data.getTeam().getFull_name());
			
			final NBAPlayerScore score = findPlayerScore(playerId, teamToUpdate.getPlayerScores());
			if (score == null) {
				NBAPlayerScore newScore = new NBAPlayerScore();
				newScore.setName(playerId);
				newScore.setScore(data.getPts());
				
				 teamToUpdate.getPlayerScores().add(newScore);
			} else {
				score.setScore(score.getScore() + data.getPts());
			}
		}

		LOG.debug("[{}] Finished creating NBAMatches. Returning metadata...", ctx.getEventId());
		return httpResponse.getBody().getMeta();
	}

	private NBAPlayerScore findPlayerScore(String playerId, List<NBAPlayerScore> playerScores) {
		if (playerScores != null) {
			for (NBAPlayerScore score: playerScores) {
				if (score.getName().equals(playerId)) {
					return score;
				}
			}
		}
		
		return null;
	}
	
	public List<NBAMatchComment> getAllNBAMatchComments(Context ctx, String gameId) throws NBAServiceException {
		try {
			LOG.info("[{}] Fetching all comments for NBAMatch with gameId \"{}\"...", ctx.getEventId(), gameId);

			final NBAMatchComment example = new NBAMatchComment();
			example.setGameId(gameId);
			
			return this.matchCommentRepository.findAll(
					Example.of(example), 
					Sort.by(Order.desc("date"))
			);
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to fetch all NBA match comments for game with id \"" + gameId + "\"", 
					e
			);
		}
	}

	public Map<String, List<NBAMatchComment>> getAllNBAMatchComments(Context ctx, Iterable<String> gameIds) throws NBAServiceException {
		try {
			LOG.info("[{}] Fetching all comments for NBAMatches with gameIds \"{}\"...", ctx.getEventId(), gameIds);
			
			final Map<String, List<NBAMatchComment>> result = new HashMap<String, List<NBAMatchComment>>();
			final List<NBAMatchComment> dbComments = this.matchCommentRepository
					.findAllByGameIdIn(gameIds, Sort.by(Order.desc("date")));
			
			if (LOG.isDebugEnabled()) {
				LOG.debug("[{}] Got all comments for game ids {}:\n{}", ctx.getEventId(), gameIds, JSONUtils.toJSON(dbComments));
			}
			
			final Iterator<NBAMatchComment> commentsIterator = dbComments.iterator();
			while (commentsIterator.hasNext()) {
				final NBAMatchComment comment = commentsIterator.next();
				commentsIterator.remove();
				
				// get comments associated to a game
				List<NBAMatchComment> comments = result.get(comment.getGameId());
				if (comments == null) {
					// comments list does not exist for the game: add
					result.put(comment.getGameId(), comments = new LinkedList<NBAMatchComment>());
				}
				
				// add comment to list
				comments.add(comment);
			}
			
			return result;
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to fetch all NBA matches comments with gameIds \"" + gameIds + "\"", 
					e
			);
		}
	}
	
	public NBAMatchComment addNBAMatchComment(Context ctx, String gameId, String comment) throws NBAServiceException {
		try {
			LOG.info("[{}] Add comment \"{}\" to NBA Match with id \"{}\" ...", ctx.getEventId(), comment, gameId);
			
			// do some pre validations
			if (gameId == null) {
				LOG.error("[{}] GameId parameter was null!", ctx.getEventId());
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, "GameId cannot be null!");
			}
			
			Optional<NBAMatch> getResult = this.matchRepository.findById(gameId);
			if (!getResult.isPresent()) {
				// try to get from FreeNBA
				final Date date = getDateForGameId(ctx, gameId);
				if (date != null) {
					if (fillDBWithDataForDate(ctx, date)) {
						getResult = this.matchRepository.findById(gameId);
					}
				}
				
				if (!getResult.isPresent()) {
					throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, "No game exists with id \"" + gameId + "\"");
				}
			}
			
			final NBAMatchComment commentInstance = new NBAMatchComment();
			commentInstance.setId(UUID.randomUUID().toString());
			commentInstance.setGameId(gameId);
			commentInstance.setComment(comment);
			
			prepareNBAMatchComment(ctx, commentInstance);
			
			this.matchCommentRepository.insert(commentInstance);
			
			processCacheFor(ctx, getResult.get());
			
			return commentInstance;
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to add comment \"" + comment + "\" to a NBA match with game id \"" + gameId + "\"", 
					e
			);
		}
	}
	
	@Transactional
	public NBAMatchComment updateNBAMatchComment(Context ctx, String gameId, String commentId, String comment) throws NBAServiceException {
		LOG.info("[{}] Update comment with id \"{}\" belonging to NBA Match with id \"{}\" to \"{}\" ...", ctx.getEventId(), commentId, gameId, comment);
		try {
			// do some pre validations
			if (gameId == null) {
				LOG.error("[{}] GameId parameter was null!", ctx.getEventId());
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, "GameId cannot be null!");
			}
			
			if (commentId == null) {
				LOG.error("[{}] CommentId parameter was null!", ctx.getEventId());
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, "CommentId cannot be null!");
			}
			
			final Optional<NBAMatch> getMatchResult = this.matchRepository.findById(gameId);
			if (!getMatchResult.isPresent()) {
				LOG.error("[{}] No NBAMatch found with id \"{}\"!", ctx.getEventId(), gameId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, "No game exists with id \"" + gameId + "\"");
			}
			
			final Optional<NBAMatchComment> getCommentResult = this.matchCommentRepository.findById(commentId);
			if (!getCommentResult.isPresent()) {
				LOG.error("[{}] No NBAMatch comment found with id \"{}\"!", ctx.getEventId(), commentId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, 
						"No comment exists with id \"" + commentId + "\" for match with game id \"" + gameId + "\""
				);
			}
			
			if (!getCommentResult.get().getGameId().equals(gameId)) {
				LOG.error("[{}] NBAMatch comment id \"{}\" does not belong to match with id \"{}\"!", ctx.getEventId(), commentId, gameId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, 
						"Match with GameId \"" + gameId + "\" does not own comment with id \"" + commentId + "\"");
			}
			
			// update comment
			getCommentResult.get().setComment(comment);
			getCommentResult.get().setDate(new Date());
			
			// save
			if (LOG.isDebugEnabled()) {
				LOG.debug("[{}] Storing NBAMatchComment:\n{}", ctx.getEventId(), JSONUtils.toJSON(getCommentResult.get()));
			}
			this.matchCommentRepository.save(getCommentResult.get());
			
			// cache game data
			processCacheFor(ctx, getMatchResult.get());
			
			// return comment modified
			return getCommentResult.get();
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to delete comment with id \"" + commentId + "\" from NBA match with game id \"" + gameId + "\"", 
					e
			);
		}
	}

	@Transactional
	public NBAMatchComment deleteNBAMatchComment(Context ctx, String gameId, String commentId) throws NBAServiceException {
		try {
			// do some pre validations
			if (gameId == null) {
				LOG.error("[{}] GameId parameter was null!", ctx.getEventId());
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, "GameId cannot be null!");
			}
			
			if (commentId == null) {
				LOG.error("[{}] CommentId parameter was null!", ctx.getEventId());
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, "CommentId cannot be null!");
			}
			
			final Optional<NBAMatch> getMatchResult = this.matchRepository.findById(gameId);
			if (!getMatchResult.isPresent()) {
				LOG.error("[{}] No NBAMatch found with id \"{}\"!", ctx.getEventId(), gameId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, "No game exists with id \"" + gameId + "\"");
			}
			
			final Optional<NBAMatchComment> getCommentResult = this.matchCommentRepository.findById(commentId);
			if (!getCommentResult.isPresent()) {
				LOG.error("[{}] No NBAMatch comment found with id \"{}\"!", ctx.getEventId(), commentId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.NONEXISTENT_ENTITY, 
						"No comment exists with id \"" + commentId + "\" for match with game id \"" + gameId + "\""
				);
			}
			
			if (!getCommentResult.get().getGameId().equals(gameId)) {
				LOG.error("[{}] NBAMatch comment id \"{}\" does not belong to match with id \"{}\"!", ctx.getEventId(), commentId, gameId);
				throw new NBAServiceException(ctx.getEventId(), ResultCode.VALIDATION_FAILURE, 
						"Match with GameId \"" + gameId + "\" does not own comment with id \"" + commentId + "\"");
			}
			
			this.matchCommentRepository.deleteById(commentId);
			
			processCacheFor(ctx, getMatchResult.get());
			
			return getCommentResult.get();
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"When attempting to delete comment with id \"" + commentId + "\" from NBA match with game id \"" + gameId + "\"", 
					e
			);
		}
	}

	private void processCacheFor(Context ctx, Collection<NBAMatch> result) throws NBAServiceException {
		try {
			LOG.debug("[{}] Processing cache for {} NBA matches...", ctx.getEventId(), result.size());
			
			final Date newTimespan = generateNewTimespan();
			
			for (NBAMatch match : result) {
				match.setTimespan(newTimespan);
			}

			LOG.debug("[{}] Bulk saving {} NBA matches...", ctx.getEventId(), result.size());
			this.matchRepository.saveAll(result);
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"when attempting to bulk save a list of NBA matches", 
					e
			);
		}
	}

	private void processCacheFor(Context ctx, NBAMatch result) throws NBAServiceException {
		try {
			result.setTimespan(generateNewTimespan());
			
			this.matchRepository.save(result);
		} catch (RuntimeException e) {
			throw new NBAServiceException(
					ctx.getEventId(), 
					ResultCode.UNCHECKED_ERROR, 
					"when attempting to save a single NBA match", 
					e
			);
		}
	}

	private Date generateNewTimespan() {
		final Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		calendar.setTimeInMillis(new Date().getTime() + (cacheConfig.getTimeToLive() * 1000L));
		final Date newTimespan = calendar.getTime();
		return newTimespan;
	}

	private static void prepareNBAMatchComment(Context ctx, NBAMatchComment comment) {
		comment.setDate(new Date());
	}

	private static Date prepareDate(Date date) {
		Date d = date != null ? date : new Date();
		
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		c.setTime(d);
		
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		
		return d;
	}
	
	private static List<NBAMatch> processNBAMatchResults(List<NBAMatch> result) {
		for (NBAMatch match : result) {
			processNBAMatchResult(match);
		}
		return result;
	}
	
	private static NBAMatch processNBAMatchResult(NBAMatch match) {
		if (match.getHomeTeam() != null) {
			filterOutZeroPlayerScores(match.getHomeTeam().getPlayerScores());
		}
		
		if (match.getVisitorTeam() != null) {
			filterOutZeroPlayerScores(match.getVisitorTeam().getPlayerScores());
		}
		
		return match;
	}

	private static void filterOutZeroPlayerScores(List<NBAPlayerScore> playerScores) {
		final Iterator<NBAPlayerScore> iterator = playerScores.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getScore() <= 0) {
				iterator.remove();
			}
		}
	}
}
