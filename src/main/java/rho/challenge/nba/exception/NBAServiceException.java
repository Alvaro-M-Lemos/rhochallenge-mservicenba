package rho.challenge.nba.exception;

import org.springframework.http.HttpStatus;

import rho.challenge.nba.constant.Constants;
import rho.challenge.nba.constant.ResultCode;

public class NBAServiceException extends Exception {
	private static final long serialVersionUID = 3721431469783390696L;
	
	private String eventId;
	private HttpStatus httpStatus;
	private String systemCode;
	
	public NBAServiceException(String eventId) {
		this(eventId, Constants.EXCEPTION_DEFAULT_ERROR_CODE);
	}
	
	public NBAServiceException(String eventId, ResultCode code) {
		this(eventId, code, null, null);
	}
	
	public NBAServiceException(String eventId, ResultCode code, Throwable cause) {
		this(eventId, code, null, cause);
	}
	public NBAServiceException(String eventId, ResultCode code, String additionalInfo) {
		this(eventId, code.getHttpStatus(), getDescription(code, additionalInfo), code.getSystemCode(), null);
	}
	
	public NBAServiceException(String eventId, ResultCode code, String additionalInfo, Throwable cause) {
		this(eventId, code.getHttpStatus(), getDescription(code, additionalInfo), code.getSystemCode(), cause);
	}
	
	public NBAServiceException(String eventId, HttpStatus httpStatus, String message, String systemCode, Throwable cause) {
		super(message, cause);
		
		this.eventId = eventId == null ? Constants.DEFAULT_EXCEPTION_EVENT_ID : null;
		this.httpStatus = httpStatus;
		this.systemCode = systemCode;
	}
	
	public String getEventId() {
		return this.eventId;
	}
	
	public HttpStatus getHttpStatus() {
		return this.httpStatus;
	}
	
	public String getSystemCode() {
		return this.systemCode;
	}
	
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		if (this.eventId != null) {
			builder.append("[evtId: ").append(this.eventId).append("]");
		}
		
		builder.append(this.getClass().getSimpleName()).append(" - [").append(this.getSystemCode()).append("] ").append(this.getMessage());
		if (this.getStackTrace().length > 0) {
			builder.append(" in method: " + this.getStackTrace()[0]);
		}
		
		Throwable e = this;
		while ((e = e.getCause()) != null) {
			builder.append("\tcaused by: ").append(e.getClass().getSimpleName()).append(" - ").append(e.getMessage());
			if (e.getStackTrace().length > 0) {
				builder.append(" in method: " + e.getStackTrace()[0]);
			}
		}
		
		return builder.toString();
	}
	
	private static String getDescription(ResultCode code, String additionalInfo) {
		if (additionalInfo != null) {
			return new StringBuilder(code.getDescription().length() + 3 + additionalInfo.length())
					.append(code.getDescription())
					.append(" [")
					.append(additionalInfo)
					.append("]")
					.toString();
		} else {
			return code.getDescription();
		}
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

}
