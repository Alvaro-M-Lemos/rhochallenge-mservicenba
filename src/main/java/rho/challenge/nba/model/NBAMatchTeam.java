package rho.challenge.nba.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class NBAMatchTeam {
	private String name;
	private int totalScore;
	private List<NBAPlayerScore> playerScores;
}
