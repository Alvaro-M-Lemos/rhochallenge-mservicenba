package rho.challenge.nba.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class NBAPlayerScore {
	private String name;
	private int score;
}
