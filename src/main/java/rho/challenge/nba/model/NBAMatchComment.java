package rho.challenge.nba.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document("NBAComment")
@Getter @Setter @NoArgsConstructor
public class NBAMatchComment {
	@Id
	private String id;
	
	@Indexed
	private String gameId;
	
	@Indexed(direction = IndexDirection.DESCENDING)
	private Date date;
	
	private String comment;
}
