package rho.challenge.nba.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document("NBAMatch")
@Getter @Setter @NoArgsConstructor
public class NBAMatch {
	@Id
	private String gameId;
	
	@Indexed
	private Date gameDate;
	
	private NBAMatchTeam homeTeam;
	private NBAMatchTeam visitorTeam;

	@Indexed(expireAfterSeconds = 0)
	private Date timespan = new Date(); 
}
