package rho.challenge.nba.http.api;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;

import rho.challenge.nba.config.ApplicationConfig;
import rho.challenge.nba.exception.NBAServiceException;
import rho.challenge.nba.http.api.response.NBAAPIResponse;
import rho.challenge.nba.utils.Context;
import rho.challenge.nba.utils.HTTPUtils;
import rho.challenge.nba.utils.Utils;

public class FreeNBAAPI {
	private static final String URL_NBA_GET_ALL_STATS = "https://free-nba.p.rapidapi.com/stats";
	
	private static final String PAGINATION_QUERY_PARAM_PAGE = "page";
	private static final String PAGINATION_QUERY_PARAM_PER_PAGE = "per_page";
	private static final String FILTER_QUERY_PARAM_DATES = "dates[]";
	private static final String FILTER_QUERY_PARAM_GAME_IDS = "game_ids[]";
	
	private static final String REQ_HEADER_X_RAPIDAPI_KEY = "x-rapidapi-key";
	private static final String REQ_HEADER_X_RAPIDAPI_HOST = "x-rapidapi-host";
	private static final String REQ_HEADER_VAL_X_RAPIDAPI_HOST = "free-nba.p.rapidapi.com";
	private static final String REQ_HEADER_USE_QUERY_STRING = "useQueryString";
	
	private final ApplicationConfig config;
	private final MultiValueMap<String, String> defaultAPIReqHeaders;

	public FreeNBAAPI(ApplicationConfig config) {
		this.config = config;
		this.defaultAPIReqHeaders = getAPIDefaultRequestHeaders();
	}
	
	public ResponseEntity<NBAAPIResponse> fetchStatsForDate(Context ctx, int page, int pageSize, Date date) throws NBAServiceException {
		return fetchStats(ctx, page, pageSize, date, null);
	}
	
	public ResponseEntity<NBAAPIResponse> fetchStatsForGameId(Context ctx, int page, int pageSize, String gameId) throws NBAServiceException {
		return fetchStats(ctx, page, pageSize, null, gameId);
	}
	
	private ResponseEntity<NBAAPIResponse> fetchStats(Context ctx, int page, int pageSize, Date date, String gameId) throws NBAServiceException {
		Map<String, String> queryParameters = new HashMap<String, String>();
		
		queryParameters.put(PAGINATION_QUERY_PARAM_PAGE, String.valueOf(page));
		queryParameters.put(PAGINATION_QUERY_PARAM_PER_PAGE, String.valueOf(pageSize));
		
		if (date != null) {
			queryParameters.put(FILTER_QUERY_PARAM_DATES, Utils.toTextualDate(date));
		}
		
		if (gameId != null) {
			queryParameters.put(FILTER_QUERY_PARAM_GAME_IDS, gameId);
		}
		
		return HTTPUtils.doGet(ctx, 
				URL_NBA_GET_ALL_STATS, 
				null, 
				queryParameters, 
				this.defaultAPIReqHeaders, 
				config.getHttpRequestTimeout(), 
				NBAAPIResponse.class
		);
	}

	private MultiValueMap<String, String> getAPIDefaultRequestHeaders() {
		Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
		
		requestHeaders.put(REQ_HEADER_X_RAPIDAPI_KEY, Arrays.asList(this.config.getNbaapi_key()));
		requestHeaders.put(REQ_HEADER_X_RAPIDAPI_HOST, Arrays.asList(REQ_HEADER_VAL_X_RAPIDAPI_HOST));
		requestHeaders.put(REQ_HEADER_USE_QUERY_STRING, Arrays.asList(Boolean.TRUE.toString()));
		
		return new MultiValueMapAdapter<String, String>(requestHeaders);
	}
}
