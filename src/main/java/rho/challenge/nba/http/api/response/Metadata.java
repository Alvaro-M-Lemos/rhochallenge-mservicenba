package rho.challenge.nba.http.api.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Metadata {	
	// we are only going to need these two values
	private int total_pages;
	private int current_page; 
}
