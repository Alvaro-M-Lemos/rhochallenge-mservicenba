package rho.challenge.nba.http.api.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Team {
	private int id;
	private String full_name;
}
