package rho.challenge.nba.http.api.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Game {
	private int id;
	private String date;
	
	private int home_team_id;
	private int home_team_score;
	private int visitor_team_id;
	private int visitor_team_score;
}
