package rho.challenge.nba.http.api.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Player {
	private int id;
	private String first_name;
	private String last_name;
}
