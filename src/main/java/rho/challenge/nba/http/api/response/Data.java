package rho.challenge.nba.http.api.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Data {
	private int id;
	
	private int pts; // points/score
	
	private Game game;
	private Player player;
	private Team team;
	
}
