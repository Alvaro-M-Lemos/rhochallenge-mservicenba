package rho.challenge.nba.http.api.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class NBAAPIResponse {
	private List<Data> data;
	private Metadata meta;
}
