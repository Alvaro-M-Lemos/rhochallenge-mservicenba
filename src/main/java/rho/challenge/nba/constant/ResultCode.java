package rho.challenge.nba.constant;

import org.springframework.http.HttpStatus;

public enum ResultCode {
	OK(HttpStatus.OK, "OK.", "NBA-SERV-0000"),
	
	UNCHECKED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown type of error!", "NBA-SERV-2000"),
	INVALID_INPUT_DATA(HttpStatus.BAD_REQUEST, "Invalid type of input data!", "NBA-SERV-2001"),
	DATA_CONVERSION_FAILED(HttpStatus.INTERNAL_SERVER_ERROR, "A data conversion process failed!", "NBA-SERV-2002"),
	VALIDATION_FAILURE(HttpStatus.BAD_REQUEST, "The input data did not validate successfully.", "NBA-SERV-2003"),
	EXISTENT_ENTITY(HttpStatus.CONFLICT, "An entity with the same identifier already exists!", "NBA-SERV-2004"),
	NONEXISTENT_ENTITY(HttpStatus.NOT_FOUND, "An entity with the specified identifier does not exist!", "NBA-SERV-2005"),
	HTTP_REQUEST_TIMEOUT(HttpStatus.REQUEST_TIMEOUT, "A HTTP request timed out related problem occurred!", "NBA-SERV-2006"),
	HTTP_REQUEST_FAILED(HttpStatus.INTERNAL_SERVER_ERROR, "A http request result was not as expected!", "NBA-SERV-2007");
	
	private HttpStatus httpStatus;
	private String desc;
	private String systemCode;
	
	private ResultCode(HttpStatus httpStatus, String desc, String systemCode) {
		this.httpStatus = httpStatus;
		this.desc = desc;
		this.systemCode = systemCode;
	}
	
	public HttpStatus getHttpStatus() {
		return this.httpStatus;
	}
	
	public String getDescription() {
		return this.desc;
	}
	
	public String getSystemCode() {
		return this.systemCode;
	}
}
