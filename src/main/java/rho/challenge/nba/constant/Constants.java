package rho.challenge.nba.constant;

public class Constants {
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DEFAULT_DATE_TIME_ZONE = "UTC";
	
	public static final String DATE_FORMAT_RFC3339NANO = "yyyy-MM-dd'T'HH:mm:ss.SSSXX";

	public static final String HTTP_HEADER_EVENT_ID = "NBA-Serv-EventId";
	public static final String HTTP_HEADER_RESULT_CODE = "NBA-Serv-ResultCode";
	public static final String HTTP_HEADER_RESULT_DESC = "NBA-Serv-ResultDescription";
	
	public static final ResultCode EXCEPTION_DEFAULT_ERROR_CODE = ResultCode.UNCHECKED_ERROR;
	
	public static final int DEFAULT_PAGE_SIZE = 25;
	
	public static final int DEFAULT_HTTP_TIMEOUT_DELAY_SECONDS = 10; // seconds
	
	public static final String DEFAULT_EXCEPTION_EVENT_ID = "NotDefined";
}
