# RHOChallenge-MServiceNBA
> RHO Technical challenge

This project was created for the RHO Technical challenge and its purpose is to be an aggregator for NBA Matches from the free API FreeNBA (https://rapidapi.com/theapiguy/api/free-nba/endpoints). 
<br>It's primary functionalities are the following:
* 1. Provide all NBA Matches for a given date (data is retrieved from free API) with filtered related data
* 2. Provide data for a NBA Match along with the comments associated to it.
* 3. Possibility of managing comments on a given NBA Match (stored on MongoDB)

The project exposes a JAVA REST web application with Spring Boot and MongoDB storage to achieve this aggregation.
<br>
<br><b>Why choose MongoDB ?</b>
<br>MongoDB is a very powerful but simple and easy BD to use for storing non related documents in a BD.
<br>Apart from being accustomed to using Mongo, its feature on auto expiring documents was perfect for caching and so was the decisive point to use it.

## Getting started

To get started please install JAVA, Docker with Docker-compose and maven as this project uses those tools to automate the installation process.
<br>Once installed do the following steps:

* 1. Clone GIT repository

```shell
git clone https://gitlab.com/Alvaro-M-Lemos/rhochallenge-mservicenba.git .
```
* 2. Build project

```shell
mvn clean install -DskipTests
```

* 3. Build the docker images:

```shell
docker-compose build
```
* 4. Start up MongoDB and the Application:

```shell
docker-compose up
```

Having done the above, if no errors occurred, you should have the application deployed in localhost:8080. 
<br>You can access it here: http://localhost:8080/api/v1/nba/

## Developing

To further develop the project the following is required:
* 1. JAVA - for compilation and to run (openjdk 11)
* 2. Maven - to install the project (generate jar)
* 3. An IDE or text program (to edit the source code)
* 4. MongoDB - the DB that is used by the Application (optional, can be accessed through docker, latest version - 4.4.4)
	
Once you have the above installed, clone the project:

```shell
git clone https://gitlab.com/Alvaro-M-Lemos/rhochallenge-mservicenba.git .
```
Once cloned, you can access the JAVA source code in <b>[projectFolder]/src/main/java/</b>

### Building
On the project root folder:

```shell
	mvn clean install
```
	
This executes a maven command to install project (build jar executable to use for deployment). 
<br>This will also execute the tests phase, if this is not desired run the same maven command but with the flag -DskipTests like the following:

```shell
	mvn clean install -DskipTests
```
This will skip the tests and still build the project's jar executable.

### Tests
During the building phase, maven will execute the tests automatically. 
<br>Depending on your machine's state it may fail in the Integration tests since you may not have a valid MongoDB instance running.
<br>To facilitate this, you can do one of the following:

* Use Docker compose to startup Mongo (this will also start the web application on port 8080)
* Install MongoDB on machine
	
If you choose the <b>option 1</b>, run the following commands:

```shell
	docker-compose build
	docker-compose up
```
This will start the MongoDB and web application in port 8080. After this, you can test the application by executing the maven command:

```shell
	mvn test
```
	
This will run all the tests including integration tests.

If you choose <b>option 2</b>, install Mongo on your machine, once installed do the following:

```shell
	mongod --replSet rs0
```
	
This will start MongoDB with a replica set named rs0. Depending on the OS, you may need to specify the db's path when starting MongoDB, like the following:

```shell
	mongod --replSet rs0 --dbpath=folder
```

Once its running, initiate replica set:

```shell
	mongo --eval "rs.initiate()"
```
	
This will initiate the replica set that we created on the start up.
<br>Once this is done, MongoDB is ready to work.
<br>
<br>The pros [+] and cons [-] of <b>option 1</b>:

* [+] No need to install mongo on your machine. 
* [+] The configuration process is automatic 
* [-] Web Application is always running too (on port 8080) 
* [-] Requires Docker 
* [-] Every change in the source code needs a recompilation which means a new build of docker image and then a restart.

The pros [+] and cons [-] of <b>option 2</b>:

* [+] Can change and test Web Application on the fly (no need to build docker image and restart it) 
* [+] Does not require Docker
* [-] Need to install mongo on your machine. 
* [-] Need to configure replica set and initiate it the first time (to enable transactions) 

### Deploying / Publishing

To deploy simply use docker to push the images to a repository, like the following:
	
```shell
docker-compose push
```

This will push an image of the project to the repository.

## Features
Once the application is running along with MongoDB, the following can be done:

* Get NBA matches for a given date
* Get data for a single NBA match
* Add, update and delete comments for a NBA match

Bellow you can find the REST requests you can do to achieve the stated above:
<br>

* <b>GET</b> http://localhost:8080/api/v1/nba/matches?date=yyyy-MM-dd&page=0&pageSize=25

> Provides games for the specified date with pagination. The pagination (page and pageSize) is not required and may not be sent on the request. 

* <b>GET</b> http://localhost:8080/api/v1/nba/matches/{gameId}

> Provides data for a NBA match with the specified game id.

* <b>POST</b> http://localhost:8080/api/v1/nba/matches/{gameId}/comments This is a comment about the game

> Adds a comment to a game bound to gameId and returns the comment instance.
		
* <b>PUT</b> http://localhost:8080/api/v1/nba/matches/{gameId}/comments/{commentId} A new comment

> Updates an existent comment on a given game.

* <b>DELETE</b> http://localhost:8080/api/v1/nba/matches/{gameId}/comments/{commentId}

> Deletes an existent comment on a given game.

All exposed endpoints return the following response headers:

* <b>NBA-Serv-ResultCode</b> - The result code of the operation.
* <b>NBA-Serv-ResultDescription</b> - The description of associated to the result code.
* <b>NBA-Serv-EventId</b> - The event id of the operation (used to identify the request of the client)

<br>In addition, this application supports cache as to reduce network traffic. 
<br>All data, that is obtained through calls to the FreeNBAAPI, is cached with a timespan and will only be available in cache for the duration of the timespan.
<br>The timespan is updated every time the same data is accessed.
<br>The timespan can be configured through the <b>application.properties</b> by changing the property <b>"cache.timeToLive"</b> and its value is in seconds.
<br>
<br>All configurations are specified in the <b>application.properties</b> file located on <b>/src/main/resources/</b>
<br>However, these configurations, specified in the application.properties, can be changed on the docker-compose ".env" file by changing the property <b>"APP_COMMAND_LINE_ARGS"</b> which allows us to skip the compilation and build steps. In other words, to change a application property:

* 1. Change .env file's APP_COMMAND_LINE_ARGS property
* 2. Stop application by hitting ctrl+c (if its running by docker-compose)
* 3. Start up application again by executing the following:

```shell
docker-compose up
```

When the application is up and running, it will assume the new configuration set by the command line argument.

## Links

- Project homepage: https://gitlab.com/Alvaro-M-Lemos/rhochallenge-mservicenba

