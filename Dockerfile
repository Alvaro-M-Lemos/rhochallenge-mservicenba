FROM openjdk:latest
COPY target/RHOChallenge-MServiceNBA.jar MServiceNBA.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/MServiceNBA.jar"]